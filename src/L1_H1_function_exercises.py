def upper_word(word):
    return word.upper()


# Przypisanie wartości do zmiennej i wydrukowanie zmiennej
big_dog = upper_word("dog")
print(big_dog)
# Wydrukowanie wyniku wykonania funkcji dla jakiegoś argumentu
print(upper_word("tree"))


def add_two_numbers(a, b):
    return a + b


c = add_two_numbers(3, 5)
print(c)


def print_a_car_brand_name():
    print("Honda")


def print_given_number_multiplied_by_3(input_number):
    print(input_number * 3)


def calculate_area_of_a_circle(radius):
    return 3.1415 * radius ** 2


def calculate_area_of_a_triangle(bottom, height):
    return 0.5 * bottom * height


print_a_car_brand_name()
print_given_number_multiplied_by_3(10)
area_of_a_circle_with_radius_10 = calculate_area_of_a_circle(10)
print(area_of_a_circle_with_radius_10)
area_a_little_triangle = calculate_area_of_a_triangle(5, 5)
print(area_a_little_triangle)

# Checking what is returned from a function by default
result_of_a_print_function = print_a_car_brand_name()
print(result_of_a_print_function)
