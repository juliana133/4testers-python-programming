import random
from datetime import datetime


def get_welcome_message(person):
    welcome_message = f'Hi! I\'m {person["firstname"]} ' \
                      f'{person["lastname"]}. ' \
                      f'I come from {person["country"]} ' \
                      f'and I was born in {person["birth_year"]}.'
    return welcome_message


if __name__ == '__main__':

    female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
    male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
    surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
    countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']

    list_of_dictionaries_with_persons = []
    for i in range(0, 10):
        if i % 2 == 0:
            random_firstname = random.choice(female_fnames)
        else:
            random_firstname = random.choice(male_fnames)
        random_surnames = random.choice(surnames)
        random_countries = random.choice(countries)
        random_email = random_firstname.lower() + '.' + random_surnames.lower() + '@example.com'
        random_age = random.randint(5, 45)
        is_adult = random_age > 18
        birth_year = datetime.now().year - random_age
        list_of_dictionaries_with_persons.append(
            {
                "firstname": random_firstname,
                "lastname": random_surnames,
                "country": random_countries,
                "email": random_email,
                "age": random_age,
                "adult": is_adult,
                "birth_year": birth_year
            }
        )

    for single_person in list_of_dictionaries_with_persons:
        print(get_welcome_message(single_person))
