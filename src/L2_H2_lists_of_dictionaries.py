# Dictionaries
animal1 = {
    "kind": "dog",
    "age": 2,
    "male": True
}
animal2 = {
    "kind": "cat",
    "age": 5,
    "male": False
}

# Lists

animal_kinds = ["dog", "cat", "fish"]

# Lists of dictionaries (3 dictionaries, each with 3 elements)
animals = [
    {
        "kind": "dog",
        "age": 2,
        "male": True
    },
    {
        "kind": "cat",
        "age": 5,
        "male": False
    },
    {
        "kind": "fish",
        "age": 1.5,
        "male": False
    }
]
print(len(animals))
print(animals[0])
print(animals[-1])
last_animal = animals[-1]
last_animal_age = last_animal["age"]
print('The age of last animal is equal to: ', last_animal_age)

# Double index
print(animals[0]["male"])
print(animals[1]["male"])

# Add an animal
animals.append(
    {
        "kind": "zebra",
        "age": 7,
        "male": False
    }
)
print(animals)