# Dictionaries

friend = {
    'name': 'Marta',
    'age': '34',
    'hobbies': ['swimming', 'reading']
}

print(friend['age'])
friend['city'] = 'Wroclaw'
friend['age'] = 87
print(friend)
friend['job'] = 'doctor'
print(friend)
del friend['job']
print(friend)
print(friend['hobbies'][-1])
print(friend)
friend['hobbies'].append('climbing')
print(friend)


def get_employee_info(email):
    return {
        'employee_email': email,
        'company': '4testers.pl'
    }


if __name__ == '__main__':
    info_1 = get_employee_info('robert@4testers.pl')
    info_2 = get_employee_info('rafal@4testers.pl')
    info_3 = get_employee_info('magda@4testers.pl')

    print(info_1)
    print(info_2)
    print(info_3)
