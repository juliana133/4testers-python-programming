# Exercise 1
import random
import string
import uuid


def get_list_element_sum(list_with_numbers):
    return sum(list_with_numbers)


def get_half_sum(number_1, number_2):
    return (number_1 + number_2) / 2


if __name__ == '__main__':
    temperature_in_january = [-4, 1.0, -7, 2]
    temperature_in_february = [-13, -9, -3, 3]
    sum_temperature_in_january = get_list_element_sum(temperature_in_january)
    sum_temperature_in_february = get_list_element_sum(temperature_in_february)
    print(f'January: {sum_temperature_in_january}')
    print(f'February: {sum_temperature_in_february}')

    half_sum_temperature_january_february = get_half_sum(sum_temperature_in_january, sum_temperature_in_february)
    print(f'The average temperature in January and February is: {half_sum_temperature_january_february}.')

# Exercise 2a

def get_random_login_data(user_email):
    user_password = uuid.uuid4()
    return {
        "Email:": user_email,
        "Password:": f"{user_password}"
    }


print(get_random_login_data("user1@example.com"))
print(get_random_login_data("user2@example.com"))
print(get_random_login_data("user3@example.com"))
print(get_random_login_data("user4@example.com"))
print(get_random_login_data("user5@example.com"))


# Exercise 2b

def get_random_password(password_len):
    random_source = string.ascii_letters + string.digits + string.punctuation
    password = random.choice(string.ascii_letters)
    password += random.choice(string.digits)
    password += random.choice(string.punctuation)
    for i in range(password_len):
        password += random.choice(random_source)

    password_list = list(password)
    random.SystemRandom().shuffle(password_list)
    password = ''.join(password_list)
    return password


def get_random_login_of_data(user_email):
    return {
        "Email": user_email,
        "Password": get_random_password(10)
    }


user_email1 = "user1@example.com"
user_email2 = "user2@example.com"
print(get_random_login_of_data(user_email1))
print(get_random_login_of_data(user_email2))

# Exercise 3

def get_description_of_player(player_dictionary):
    player_name = player_dictionary["nick"]
    player_type = player_dictionary["type"]
    player_exp = player_dictionary["exp-points"]
    return f'The player is {player_name} of type {player_type} and has {player_exp} EXP'


def print_gamer_description(gamer_dictionary):
    print(f"The player {gamer_dictionary['nick']} is of type {gamer_dictionary['type']} and has {gamer_dictionary['exp_points']} EXP.")

player = {
    "nick": "maestro_54",
    "type": "warrior",
    "exp_points": 3000
}

print_gamer_description(player)

