import uuid


def print_random_uuids(number_of_strings):
    for i in range(number_of_strings):
        print(str(uuid.uuid4()))


def print_numbers_from_20_to_30():
    for number in range(20, 31):
        print(number)


def print_numbers_from_20_to_30_multiplied_by_4():
    for number in range(20, 31):
        print(number*4)


def print_numbers_divisible_by_7(from_number, to_number):
    for number in range(from_number, to_number + 1):
        if number % 7 == 0:
            print(number)


def print_temperatures_in_both_scales(list_of_temps_in_celsius):
    for temp in list_of_temps_in_celsius:
        print(f'Celsius: {temp}, Fahrenheit: {temp * 9 / 5 + 32}')


def get_temperature_higher_than_20_degrees(list_of_temps_in_celsius):
    filtered_temperatures = []
    for temp in list_of_temps_in_celsius:
        if temp > 20:
            filtered_temperatures.append(temp)
    return filtered_temperatures


if __name__ == '__main__':
    print_random_uuids(3)
    print_numbers_from_20_to_30()
    print_numbers_from_20_to_30_multiplied_by_4()
    print_numbers_divisible_by_7(1, 30)

    temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    print_temperatures_in_both_scales(temps_celsius)

    print(get_temperature_higher_than_20_degrees(temps_celsius))
