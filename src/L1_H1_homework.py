# Exercise 1
def get_number_squared(number):
    return number ** 2


zero_squared = get_number_squared(0)
print(zero_squared)

sixteen_squared = get_number_squared(16)
print(sixteen_squared)

floating_number_squared = get_number_squared(2.55)
print(floating_number_squared)

print(get_number_squared(0), get_number_squared(16), get_number_squared(2.55))


# Exercise 2
def calculate_volume_of_cuboid(a, b, c):
    return a * b * c


print(calculate_volume_of_cuboid(3, 5, 7))

side_one = 3
side_two = 5
side_three = 7

volume_of_cuboid = calculate_volume_of_cuboid(side_one, side_two, side_three)
print("The volume of cuboid is: ", volume_of_cuboid)


# Exercise 3
def convert_celsius_degrees_to_fahrenheit(temperature_in_celsius):
    return 32 + 9 / 5 * temperature_in_celsius


print(convert_celsius_degrees_to_fahrenheit(20))

temperature = 20
temperatures_in_fahrenheit = convert_celsius_degrees_to_fahrenheit(temperature)
print(temperatures_in_fahrenheit)
