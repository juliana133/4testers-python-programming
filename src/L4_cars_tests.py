from L4_cars import get_country_of_a_car_brand


def test_get_country_for_a_japaneese_car():
    assert get_country_of_a_car_brand('Toyota') == 'Japan'


def test_get_country_for_a_japaneese_car_lowercase():
    assert get_country_of_a_car_brand('toyota') == 'Japan'


def test_get_country_for_a_german_car_lowercase():
    assert get_country_of_a_car_brand('BMW') == 'Germany'


def test_get_country_for_a_german_car():
    assert get_country_of_a_car_brand('bmw') == 'Germany'


def test_gas_usage_for_zero_distance_driven():
    assert get_gas_usage_for_distance_driven(0, 8.5) == 0


def test_gas_usage_for_100_kilometers_distance_driven():
    assert get_gas_usage_for_distance_driven(100, 8.5) == 8.5