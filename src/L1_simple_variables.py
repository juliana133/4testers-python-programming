friend_name = "Edyta"
friend_age = 45
friend_pets = 1
friend_has_drive_licence = True
friendship_time = 19.5

print(friend_name)
print(friend_age)
print(friend_pets)
print(friend_has_drive_licence)
print(friendship_time)

print(friend_name, friend_age, friend_pets, friend_has_drive_licence, friendship_time, sep="\t")
print(friend_name, friend_age, friend_pets, friend_has_drive_licence, friendship_time, sep="; ")
print(friend_name, friend_age, friend_pets, friend_has_drive_licence, friendship_time, sep="\n")
print("Friend's name: ", friend_name, "\nFriend's age: ", friend_age, "\nFriend's pet: ", friend_pets, "\nHas driving licence? ", friend_has_drive_licence, "\nFriendship time: ", friendship_time, sep="\n")
