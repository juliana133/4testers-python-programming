if __name__ == '__main__':
    name = 'Aan'

    if len(name) > 5:
        print(f'The name {name} is longer than 5 characters.')
    else:
        print(f'The name {name} is 5 characters or shorter.')


def check_temperature_and_pressure(temperature, pressure):
    if temperature == 0 and pressure == 1013:
        return True
    else:
        return False


def get_grade_for_exam_points(number_of_points):
    if number_of_points >= 90:
        return 5
    elif number_of_points >= 75:
        return 4
    elif number_of_points >= 50:
        return 3
    else:
        return 2


if __name__ == '__main__':

    print(check_temperature_and_pressure(0, 1013))
    print(check_temperature_and_pressure(1, 1013))
    print(check_temperature_and_pressure(0, 1014))
    print(check_temperature_and_pressure(1, 1014))

    print(get_grade_for_exam_points(91))
    print(get_grade_for_exam_points(90))
    print(get_grade_for_exam_points(89))
    print(get_grade_for_exam_points(76))
    print(get_grade_for_exam_points(75))
    print(get_grade_for_exam_points(74))
    print(get_grade_for_exam_points(50))
    print(get_grade_for_exam_points(49))
