# Exercise 1

movies = ['Mamma Mia', 'Titanic', 'Zielona mila', 'Gdzie śpiewają raki', 'American Pie']

first_movie = movies[0]
print(f"First movie is {first_movie}")

last_movie = movies[-1]
print(f"Last movie is {movies[-1]}")

# metoda
movies.append("Star Trek")
print(f"The length of movies list is {len(movies)}")

movies.insert(0, "Star Wars")
movies.remove("Titanic")
print(movies)

# zastąpienie elementu w liście
movies[-1] = "Dune"
print(movies)


# Exercise 2

emails = ['a@example.com', 'b@example.com']
print(f"The length of emails list is {len(emails)}.")
print(f"First email is {emails[0]}.")
print(f"Last email is {emails[-1]}.")
emails.append("cde@example.com")
print(emails)

print(f'The length of word maksymalistycznie is {len("maksymalistycznie")}.')
