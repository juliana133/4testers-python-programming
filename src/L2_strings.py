# Exercise 1 - formatting f-string

my_name = "Jan"
favourite_movie = "Star Wars"
favourite_actor = "Brad Pitt"

print("My name is ", my_name, ". My favourite movie is ", favourite_movie,
      ". My favourite actor is ", favourite_actor, ".", sep="")

bio = f"My name is {my_name}. My favourite movie is {favourite_movie}. My favourite actor is {favourite_actor}."
print(bio)

calculation_example = f"Sum of 2 and 5 is {2+5}."
print(calculation_example)


# Exercise 2 - Welcome message

def print_welcome_message(name, city):
    print(f"Witaj {name.capitalize()}! Miło Cię widzieć w naszym mieście: {city.capitalize()}!")


print_welcome_message("Michał", "toruń")
print_welcome_message("beata", "Gdynia")


# Exercize 3 - Mail generate

def get_mail_address(name, surname):
    return f"{name.lower()}.{surname.lower()}@4testers.pl"


print(get_mail_address("Janusz", "Nowak"))
print(get_mail_address("Barbara", "Kowalska"))

# Escaping characters
restaurants = ['KFC', 'Burger King', 'McDonald\'s']
